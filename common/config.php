 <?php
return [
    // 数据库连接配置
    'DB_CONNECT' => [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '123456',
        'dbname' => 'php_fun',
        'port' => '3306'
    ],
    // 数据库字符集
    'DB_CHARSET' => 'utf8',
    // 项目的数据库表前缀
    'DB_PREFIX' => 'fun_',
];